import React, { Component } from 'react';
import {
  AppRegistry
} from 'react-native';

// import JestaApp from './src/jestaApp/jestaApp.component';
import Root from './src/root';

AppRegistry.registerComponent('jesta_alpha', () => Root);
