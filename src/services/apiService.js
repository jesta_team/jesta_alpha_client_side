class API {
    getJestas() {
        return require('../statics/jestas.json');
    }

    getUserProfile(id) {
        return require('../statics/profile.json');
    }
}

export default API;