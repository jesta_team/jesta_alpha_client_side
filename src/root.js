import React, { Component } from 'react';
// import { Provider } from 'react-redux';

// import App from './containers/App';
// import configureStore from './store/configureStore';

import FeedContainer from './containers/feedContainer';
import ProfileContainer from './containers/profileContainer';
import Jesta from './components/jesta';
import Test from './components/test';

import { COLOR, ThemeProvider } from 'react-native-material-ui';

// you can set your style right here, it'll be propagated to application
const uiTheme = {
    palette: {
        primaryColor: COLOR.green500,
    },
    toolbar: {
        container: {
            height: 50,
        },
    },
};

class Root extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            jestas: require('./statics/jestas.json')
        }
    }

    render() {
        {/*<ThemeProvider uiTheme={uiTheme}>
                <Test />
            </ThemeProvider>*/}

        return (
            // <Jesta jesta={this.state.jestas[0]} edit={false} />
            <FeedContainer />
            // <ProfileContainer />
            
        );
    }
}

export default Root;