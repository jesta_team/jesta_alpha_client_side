import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  StyleSheet,
} from 'react-native';

import { Subheader, Button, Avatar, Card, ListItem, Badge, Icon } from 'react-native-material-ui';

class Test extends Component {
    render() {
        return (
            <ScrollView style={styles.container}>
                <View style={styles.rowContainer}>
                    <View style={styles.badgeContainer}>
                        <Badge text="3" >
                            <Icon name="star" />
                        </Badge>
                    </View>
                    <View style={styles.badgeContainer}>
                        <Badge
                            text="13"
                            accent
                        >
                            <Icon name="person" />
                        </Badge>
                    </View>
                    <View style={styles.badgeContainer}>
                        <Badge accent >
                            <Icon name="warning" />
                        </Badge>
                    </View>
                    <View style={styles.badgeContainer}>
                        <Badge
                            text="13"
                            accent
                            style={{ container: { bottom: -8, right: -12 } }}
                        >
                            <Icon name="star" />
                        </Badge>
                    </View>
                </View>
                <Subheader text="Badge with button" />
                <View style={styles.rowContainer}>
                    <View style={styles.badgeContainer}>
                        <Badge accent text="8" >
                            <Button text="Flat" />
                        </Badge>
                    </View>
                    <View style={styles.badgeContainer}>
                        <Badge
                            text="5"
                            accent
                            style={{ container: { top: -12, right: -20 } }}
                        >
                            <Button
                                raised
                                primary
                                text="Raised"
                            />
                        </Badge>
                    </View>
                </View>
                <Subheader text="Badge with text" />
                <View style={styles.rowContainer}>
                    <View style={styles.badgeContainer}>
                        <Badge text="2" >
                            <Text style={{ padding: 8 }}>Text badge</Text>
                        </Badge>
                    </View>
                </View>
                <Subheader text="Badge with icon" />
                <View style={styles.rowContainer}>
                    <View style={styles.badgeContainer}>
                        <Badge
                            size={24}
                            icon="star"
                            style={{ container: { bottom: -8, right: -12 } }}
                        >
                            <Avatar text="BR" />
                        </Badge>
                    </View>
                    <View style={styles.badgeContainer}>
                        <Badge
                            size={24}
                            accent
                            icon={{
                                name: 'speaker-notes',
                                color: 'white',
                            }}
                            style={{ container: { top: -8, right: -12 } }}
                        >
                            <Avatar text="TR" />
                        </Badge>
                    </View>
                </View>
                <Subheader text="Badge with strokes" />
                <View style={styles.rowContainer}>
                    <View style={styles.badgeContainer}>
                        <Badge
                            size={24}
                            stroke={4}
                            icon="star"
                            style={{ strokeContainer: { bottom: -8, right: -12 } }}
                        >
                            <Avatar text="BR" />
                        </Badge>
                    </View>
                    <View style={styles.badgeContainer}>
                        <Badge
                            size={24}
                            stroke={8}
                            accent
                            icon={{
                                name: 'speaker-notes',
                                color: 'white',
                            }}
                            style={{ strokeContainer: { top: -8, right: -12 } }}
                        >
                            <Avatar text="TR" />
                        </Badge>
                    </View>
                    <View style={styles.badgeContainer}>
                        <Badge
                            accent
                            size={12}
                            stroke={4}
                            style={{ strokeContainer: { top: 0, right: 0 } }}
                        >
                            <Icon name="notifications" />
                        </Badge>
                    </View>
                </View>

                <View style={styles.rowContainer}>
                    <View style={styles.button}>
                        <Button primary text="Primary" />
                    </View>
                    <View style={styles.button}>
                        <Button accent text="Accent" />
                    </View>
                </View>
                <View style={styles.rowContainer}>
                    <View style={styles.button}>
                        <Button text="Default" />
                    </View>
                    <View style={styles.button}>
                        <Button disabled text="Disabled" />
                    </View>
                </View>
                <View style={styles.rowContainer}>
                    <View style={styles.button}>
                        <Button raised primary text="Primary" />
                    </View>
                    <View style={styles.button}>
                        <Button raised accent text="Accent" />
                    </View>
                </View>
                <View style={styles.rowContainer}>
                    <View style={styles.button}>
                        <Button raised text="Default" />
                    </View>
                    <View style={styles.button}>
                        <Button raised disabled text="Disabled" />
                    </View>
                </View>
                <View style={styles.rowContainer}>
                    <View style={styles.button}>
                        <Button primary text="Accept" icon="done" />
                    </View>
                    <View style={styles.button}>
                        <Button accent text="Dismiss" icon="clear" />
                    </View>
                </View>
                <View style={styles.rowContainer}>
                    <View style={styles.button}>
                        <Button raised primary text="Done" icon="done" />
                    </View>
                    <View style={styles.button}>
                        <Button raised accent text="Clear" icon="clear" />
                    </View>
                </View>

                <View style={styles.avatarContainer}>
                    <Avatar text="C" />
                </View>
                <View style={styles.avatarContainer}>
                    <Avatar icon="person" />
                </View>

                <Card>
                    <ListItem
                        leftElement={<Avatar text="JM" />}
                        centerElement={{
                            primaryText: 'John Mitri',
                            secondaryText: '3 weeks ago',
                        }}
                    />
                    <View style={styles.textContainer}>
                        <Text>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem
                            accusantium doloremque laudantium,
                            totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et
                            quasi architecto beatae vitae dicta sunt explicabo.
                        </Text>
                    </View>
                </Card>
                <Card>
                    <ListItem
                        leftElement={<Avatar text="MW" />}
                        centerElement={{
                            primaryText: 'Mike Wiliams',
                            secondaryText: '4 weeks ago',
                        }}
                    />
                    <View style={styles.textContainer}>
                        <Text>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                            sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                            Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi
                            ut aliquip ex ea commodo consequat.
                        </Text>
                    </View>
                </Card>
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        flexDirection: 'column',
    },
    rowContainer: {
        margin: 8,
        flexDirection: 'row',
        justifyContent: 'center',
    },
    button: {
        marginHorizontal: 8,
    },
    textContainer: {
        paddingHorizontal: 16,
        paddingBottom: 16,
    },
    badgeContainer: {
        paddingLeft: 24,
    },
});

export default Test;