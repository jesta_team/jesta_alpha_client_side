import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Text,
    Image,
    View,
} from 'react-native';

class CategoryElement extends Component {
    constructor(props) {
        super(props);

    }

    onPicked() {
        this.props.onPicked(this.props.category);
    }

    render() {
        return (
            <TouchableHighlight disabled={!this.props.edit} onPress={this.onPicked.bind(this)}>
                <View style={[styles.skill, (this.props.edit && this.props.category.picked) && styles.picked]}>
                    <Image source={{uri: this.props.category.uri}} style={styles.avatar} />
                    <Text style={styles.content}>{this.props.category.text}</Text>
                </View>
            </TouchableHighlight>
        )
    }
}

const styles = StyleSheet.create({
    skill: {
        // flex: 1,
        // flexDirection: 'column',
        alignItems: 'center',
        width: 80,
        height: 80,
    },
    picked: {
        backgroundColor: 'yellow',
    },
    content: {
        textAlign: 'center',
        color: '#333333',
        margin: 5,
    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 10,
        margin: 5,
    },
});

export default CategoryElement;