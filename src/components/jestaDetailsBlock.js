import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableHighlight,
  TextInput,
  ListView,
  Picker,
  DatePickerAndroid,
} from 'react-native';
import DatePicker from 'react-native-datepicker';

class JestaDetailsBlock extends Component {
    constructor(props) {
        super(props);

        this.state = this.props.details;
    }

    renderReadOnly() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <Text style={styles.title}>Title: </Text>
                    <Text style={styles.content}>{this.state.title}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Description: </Text>
                    <Text style={styles.content}>{this.state.description}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Date: </Text>
                    <Text style={styles.content}>{this.state.date}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Duration: </Text>
                    <Text style={styles.content}>{this.state.duration}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Location: </Text>
                    <Text style={styles.content}>{this.state.location}</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Participants: </Text>
                    <Text style={styles.content}>{this.state.participants}</Text>
                </View>
            </View>
        );
    }

// <TextInput 
//                     style={[styles.input, this.props.editMode && styles.editInput]}
//                     placeholder={this.props.placeholder}
//                     editable={this.props.editMode}
//                     multiline={this.props.multiline}
//                     value={this.props.value}
//                 />

    renderEditable() {
        return (
            <View style={styles.container}>
                <View style={styles.row}>
                    <Text style={styles.title}>Title: </Text>
                    <TextInput style={styles.content} editable={true} multiline={false} value={this.state.title} 
                        onChangeText={(text) => { this.setState({ title: text })}} />
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Description: </Text>
                    <TextInput style={styles.content} editable={true} multiline={true} value={this.state.description} 
                        onChangeText={(text) => { this.setState({ description: text })}} />
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Date: </Text>
                    <DatePicker
                        style={styles.dateContent}
                        date={this.state.date}
                        mode="datetime"
                        placeholder="select date"
                        format="DD/MM/YYYY HH:mm"
                        minDate="01/01/2017"
                        maxDate="31/12/2020"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        onDateChange={(date) => {this.setState({date: date})}}
                    />
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Duration: </Text>
                    <Picker style={styles.dateContent}
                        selectedValue={this.state.duration}
                        onValueChange={(dur) => this.setState({duration: dur})}>
                        <Picker.Item label="10 minutes" value="10 mins" />
                        <Picker.Item label="20 minutes" value="20 mins" />
                        <Picker.Item label="30 minutes" value="30 mins" />
                        <Picker.Item label="40 minutes" value="40 mins" />
                        <Picker.Item label="50 minutes" value="50 mins" />
                        <Picker.Item label="60 minutes" value="60 mins" />
                    </Picker>
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Location: </Text>
                    <Text style={styles.content}>NOT YET</Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.title}>Participants: </Text>
                    <Picker style={styles.dateContent}
                        selectedValue={this.state.participants}
                        onValueChange={(par) => this.setState({participants: par})}>
                        <Picker.Item label="1" value="1" />
                        <Picker.Item label="2" value="2" />
                        <Picker.Item label="3" value="3" />
                        <Picker.Item label="4" value="4" />
                        <Picker.Item label="5" value="5" />
                    </Picker>
                </View>
            </View>
        );
    };

    render() {
        if (this.props.edit) {
            return this.renderEditable();
        } else {
            return this.renderReadOnly();
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        backgroundColor: 'lightgrey',
        borderRadius: 5,
    },
    title: {
        flex: 1,
        color: 'blue',
        textAlign: 'center',
        fontWeight: 'bold',
    },
    dateContent: {
        flex: 3,
    },
    content: {
        flex: 3,
        textAlign: 'center',
        color: '#333333',
        // margin: 5,
    },
    row: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
    },
});

export default JestaDetailsBlock;