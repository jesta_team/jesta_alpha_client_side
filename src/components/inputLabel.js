import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
} from 'react-native';

class InputLabel extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={styles.label}>
                    {this.props.label}
                </Text>
                <TextInput 
                    style={[styles.input, this.props.editMode && styles.editInput]}
                    placeholder={this.props.placeholder}
                    editable={this.props.editMode}
                    multiline={this.props.multiline}
                    value={this.props.value}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    label: {
        flex: 1,
        textAlign: 'right',
        fontSize: 14,
        color: 'white',
        fontWeight: 'bold',
        marginRight: 5
    },
    input: {
        flex: 2,
        textAlign: 'left',
    },
    editInput: {
        backgroundColor: 'red',
    },
});

export default InputLabel;