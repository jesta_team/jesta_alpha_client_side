import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    block: {
        flex: 0,
        flexDirection: 'column',
        justifyContent: 'flex-start',
        margin: 5,
        backgroundColor: 'lightgrey',
        borderRadius: 5
    },
    row: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profilePic: {
        width: 50,
        height: 50,
        borderRadius: 10,
        margin: 5,
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    header: {
        textAlign: 'center',
        fontSize: 14,
        color: 'white',
        fontWeight: 'bold',
    },
    content: {
        textAlign: 'center',
        color: '#333333',
        margin: 5,
    },
    textRed: {
        color: 'red'
    },
    button: {
        backgroundColor: 'lightblue',
        margin: 5,
        borderWidth: 1,
        borderRadius: 5
    }
});