import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableHighlight,
  TextInput,
  ListView,
} from 'react-native';

// import InputLabel from './inputLabel.component';
import CategoriesBlock from '../categoriesBlock';
import UserDetailsBlock from '../userDetailsBlock';
import JestaDetailsBlock from '../jestaDetailsBlock';

import styles from './styles';

class Jesta extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            edit: this.props.edit,
            jesta: this.props.jesta,
        };
    }

    renderOwnerDetails() {
        if (!this.state.edit) {
            return <UserDetailsBlock user={this.props.jesta.owner} />;
        } else {
            return null;
        }
    }

    render() {
        return (
            <ScrollView style={styles.container}>
                <TouchableHighlight style={styles.button} onPress={() => { this.setState({edit: !this.state.edit}) }}>
                    <Text style={styles.title}>Edit</Text>
                </TouchableHighlight>
                {this.renderOwnerDetails()}
                <JestaDetailsBlock details={this.state.jesta.details} edit={this.state.edit} />
                <CategoriesBlock picked={this.state.jesta.skills} edit={this.state.edit} />
                <View style={[styles.block, styles.row]}>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Contact</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Save</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Follow</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Share</Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>
        );
    }
}

export default Jesta;