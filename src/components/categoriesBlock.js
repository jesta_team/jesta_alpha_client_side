import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableHighlight,
  TextInput,
  ListView,
} from 'react-native';

import CategoryElement from './categoryElement';

// props:
//     picked
//     edit

class CategoriesBlock extends Component {
    constructor(props) {
        super(props);

        let cats = require('../statics/categories.json');
        
        this.state = {
            categories: cats.map((cat) => { 
                let newCat = Object.assign({}, cat);
                newCat.picked = (this.props.picked.findIndex((p) => { return p.id == newCat.id }) > -1);
                return newCat;
            }),
            pickedCategories: this.props.picked,
        }
    }
    
    onCategoryPicked(pick) {
        console.log(pick);
        // let index = this.state.categories.findIndex((cat) => { return pick.id == cat.id });
        let newCats = this.state.categories.map((cat) => {
            let newCat = Object.assign({}, cat);
            if (newCat.id == pick.id) {
                newCat.picked = !newCat.picked;
            }
            return newCat;
        });
        this.setState({ categories: newCats });
    }

    renderReadOnly() {
        return this.state.categories.map((cat) => {
            if (cat.picked) {
                return (
                    <CategoryElement 
                        category={cat}
                        onPicked={this.onCategoryPicked.bind(this)} 
                        edit={false}
                    />
                );
            } else {
                return null;
            }
        });
    }

    renderPicker() {
        return this.state.categories.map((cat) => {
            return (
                <CategoryElement 
                    category={cat}
                    onPicked={this.onCategoryPicked.bind(this)}
                    edit={true}
                />
            );
        });
    }

    render() {
        if (this.props.edit) {
            return (
                <View style={styles.container}>
                    {this.renderPicker()}
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    {this.renderReadOnly()}
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        // flex: 1,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        backgroundColor: 'lightgrey',
        borderRadius: 5,
        flexWrap: 'wrap',
    },
    skill: {
        // flex: 1,
        // flexDirection: 'column',
        alignItems: 'center',
        width: 80,
        height: 80,
    },
    content: {
        textAlign: 'center',
        color: '#333333',
        margin: 5,
    },
    avatar: {
        width: 40,
        height: 40,
        borderRadius: 10,
        margin: 5,
    },
});

export default CategoriesBlock;