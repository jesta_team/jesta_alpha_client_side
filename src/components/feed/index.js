import React, { Component } from 'react';
import {
  Text,
  View,
  Image,
  TouchableHighlight,
  ListView,
  Modal
} from 'react-native';

import JestaBox from '../jestaBox';
import Jesta from '../jesta';

import styles from './styles';

class Feed extends Component {
    constructor(props) {
        super(props);
        
        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            jestasToShow: ds.cloneWithRows(this.props.jestas),
            showModal: false,
            selectedJesta: {},
        };
    }

    expandJesta(jesta) {
        this.setState({
            showModal: true,
            selectedJesta: jesta,
        });
    }

    renderRow(jesta) {
        return (
            <JestaBox 
                jesta={jesta} 
                onExpand={this.expandJesta.bind(this)}
            />
        );
    }

    render() {
        return (
            <View style={styles.container}>
                <Modal
                    animationType={"slide"}
                    transparent={false}
                    visible={this.state.showModal}
                    onRequestClose={() => {alert("Modal has been closed.")}}
                >
                    <View style={{ flex: 1, justifyContent: 'center' }}>
                        {/*<JestaBox style={{ flex: 0 }} jesta={this.state.selectedJesta} isExpand={true} />*/}
                        <Jesta jesta={this.state.selectedJesta} edit={false} />
                        <TouchableHighlight 
                            style={[styles.addButton, { flex: 0 }]}
                            onPress={() => { this.setState({ showModal: false })}}>
                            <Text>Close</Text>
                        </TouchableHighlight>
                    </View>
                </Modal>
                <TouchableHighlight style={styles.addButton}>
                    <Text style={styles.title}>New Jesta</Text>
                </TouchableHighlight>
                <ListView
                    dataSource={this.state.jestasToShow}
                    renderRow={this.renderRow.bind(this)}
                />
            </View>
        );
    }
}

export default Feed;


// MODAL RELATED STUFF

                

    