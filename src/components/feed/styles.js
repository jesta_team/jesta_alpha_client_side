import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    addButton: {
        backgroundColor: 'lightblue',
        margin: 5,
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderRadius: 5
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
});