import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableHighlight,
  TextInput,
} from 'react-native';

import InputLabel from '../inputLabel';

import styles from './styles';

class Profile extends Component {
    constructor(props) {
        super(props);

        this.state = {
            editMode: false,
            profile: this.props.profile
        };
    }

    renderSkills(skills) {
        return skills.map((skill) => {
            // var icon = require(skill.icon);
            return (
                <View style={styles.row}>
                    <Image style={styles.profilePic}
                        source={skill.icon} />
                    <Text style={styles.header}>
                        {skill.text}
                    </Text>
                </View>
            );
        });
    }

    renderEquipment(equipment) {
        return equipment.map((thing) => {
            return (
                <View style={styles.row}>
                    <Image style={styles.profilePic}
                        source={thing.icon} />
                    <Text style={styles.header}>
                        {thing.text}
                    </Text>
                </View>
            );
        });
    }

    renderActivity(jestas) {
        return jestas.map((jesta) => {
            return (
                <View style={styles.row}>
                    <Image style={styles.profilePic} source={jesta.owner.avatar} />
                    <Text style={styles.content}>{jesta.owner.name}</Text>
                    <Text style={styles.content}>{jesta.title}</Text>
                </View>
            );
        });
    }

    renderBadges(badges) {
        return badges.map((badge) => {
            return (
                <View style={styles.row}>
                    <Image style={styles.profilePic}
                        source={badge.icon} />
                    <Text style={styles.header}>
                        {badge.text}
                    </Text>
                </View>
            );
        });
    }

    render() {
        let checkIcon = require('../../../assets/images/check.png');
        let xIcon = require('../../../assets/images/x.png');

        return (
            <ScrollView style={styles.container}>
                <TouchableHighlight 
                    style={styles.row}
                    onPress={() => { this.setState({ editMode: !this.state.editMode })}}
                >
                    <Text>Edit</Text>
                </TouchableHighlight>
                <View style={styles.block}>
                    <View style={styles.row}>
                        <Image 
                            source={require('../../../assets/images/profile_blank2.jpg')}
                            style={styles.profilePic} 
                        />
                        <Text style={styles.title}>
                            {this.state.profile.about.name}
                        </Text>
                    </View>
                    <InputLabel editMode={this.state.editMode} multiline={true}
                        label='About Me:' placeholder='Tell us about yourself...'
                        value={this.state.profile.about.description} />
                    <InputLabel editMode={this.state.editMode} multiline={true}
                        label='Why am I here?' placeholder='Your reasons for Jesting...'
                        value={this.state.profile.about.reason} />
                    <InputLabel editMode={this.state.editMode} multiline={true}
                        label='My experience:' placeholder='A history of man...'
                        value={this.state.profile.about.experience} />
                    <InputLabel editMode={this.state.editMode} multiline={true}
                        label='Area of Living:' placeholder='A small place in a town called...'
                        value={this.state.profile.about.address.city} />
                </View>
                <View style={styles.block}>
                    <View style={styles.row}>
                        <Image 
                            source={require('../../../assets/images/social/facebook.png')}
                            style={styles.profilePic}
                        />
                        <Image 
                            source={require('../../../assets/images/social/twitter.png')}
                            style={styles.profilePic}
                        />
                        <Image 
                            source={require('../../../assets/images/social/instagram.png')}
                            style={styles.profilePic}
                        />
                        <Image 
                            source={require('../../../assets/images/social/linkedin.png')}
                            style={styles.profilePic}
                        />
                        <Image 
                            source={require('../../../assets/images/social/pinterest.png')}
                            style={styles.profilePic}
                        />
                    </View>
                </View>
                <View style={styles.block}>
                    <Text style={styles.header}>Skills</Text>
                    <View style={styles.row}>
                        {this.renderSkills(this.state.profile.skills)}
                    </View>
                </View>
                <View style={styles.block}>
                    <Text style={styles.header}>Special Equipment</Text>
                    <View style={styles.row}>
                        {this.renderEquipment(this.state.profile.equipment)}
                    </View>
                    
                </View>
                <View style={styles.block}>
                    <Text style={styles.header}>Activity</Text>
                    <View style={styles.row}>
                        {this.renderActivity(this.state.profile.activity)}
                    </View>
                </View>
                <View style={styles.block}>
                    <Text style={styles.header}>Badges</Text>
                    <View style={styles.row}>
                        {this.renderBadges(this.state.profile.badges)}
                    </View>
                </View>
                <View style={styles.block}>
                    <Text style={styles.header}>Verification</Text>
                    <View style={styles.row}>
                        <Image style={styles.profilePic} source={this.state.profile.verification.name ? checkIcon : xIcon} />
                        <Text style={styles.content}>Name</Text>
                    </View>
                    <View style={styles.row}>
                        <Image style={styles.profilePic} source={this.state.profile.verification.address ? checkIcon : xIcon} />
                        <Text style={styles.content}>Address</Text>
                    </View>
                    <View style={styles.row}>
                        <Image style={styles.profilePic} source={this.state.profile.verification.phoneNumber ? checkIcon : xIcon} />
                        <Text style={styles.content}>Phone Number</Text>
                    </View>
                    <View style={styles.row}>
                        <Image style={styles.profilePic} source={this.state.profile.verification.id ? checkIcon : xIcon} />
                        <Text style={styles.content}>ID</Text>
                    </View>
                </View>
            </ScrollView>
        );
    }
}

export default Profile;