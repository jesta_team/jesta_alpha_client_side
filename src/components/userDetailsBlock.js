import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  Image,
  TouchableHighlight,
  TextInput,
  ListView,
} from 'react-native';

class UserBox extends Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <View style={styles.container}>
                <Image 
                    source={require('../../assets/images/profile_blank2.jpg')}
                    style={styles.avatar} 
                />
                <View style={styles.column}>
                    <Text style={styles.title}>
                        {this.props.user.name}
                    </Text>
                    <View style={styles.row}>
                        <Text style={styles.content}>Rating: </Text>
                        <Text style={[styles.content, { color: 'green' }]}>{this.props.user.rating}</Text>
                    </View>
                </View>
                
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 0,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        margin: 5,
        backgroundColor: 'lightgrey',
        borderRadius: 5,
    },
    column: {
        flex: 0,
        flexDirection: 'column',
        marginLeft: 5
    },
    row: {
        flex: 0,
        flexDirection: 'row',
    },
    title: {
        fontSize: 20,
        textAlign: 'center',
        // margin: 10,
    },
    content: {
        textAlign: 'center',
        color: '#333333',
        // margin: 5,
    },
    avatar: {
        width: 50,
        height: 50,
        borderRadius: 10,
        margin: 5,
    },
});

export default UserBox;