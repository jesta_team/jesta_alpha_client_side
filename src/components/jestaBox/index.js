import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableHighlight,
  ListView
} from 'react-native';

import styles  from './styles';

class JestaBox extends Component {
    constructor(props) {
        super(props);

        const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.state = {
            skills: ds.cloneWithRows(this.props.jesta.skills),
        };


        // this.state = {
        //     isExpand: false
        // };
    }

    onExpand() {
        this.props.onExpand(this.props.jesta);
        // this.setState({ isExpand: !this.state.isExpand });
    }

    renderSkillRow(skill) {
        return (
            <Text style={styles.content}>{skill}</Text>
        );
    }

    renderExpansion() {
        if (this.props.isExpand) {
            return (
                <View>
                    <View style={styles.row}>
                        <Text style={styles.content}>
                            {this.props.jesta.description}
                        </Text>
                    </View>
                    <View style={styles.row}>
                        <Text style={styles.content}>
                            {this.props.jesta.date}
                        </Text>
                        <Text style={styles.content}>
                            {this.props.jesta.duration}
                        </Text>
                        <Text style={styles.content}>
                            {this.props.jesta.participants}
                        </Text>
                    </View>
                    <View style={styles.row}>
                        <ListView
                            dataSource={this.state.skills}
                            renderRow={(skill) => { return (<Text style={styles.content}>{skill.text}</Text>); }}
                        />
                    </View>
                </View>
            );
        } else {
            return null;
        }
    }

    renderExpandButton() {
        if (!this.props.isExpand) {
            return (
                <TouchableHighlight style={styles.button} onPress={this.onExpand.bind(this)}>
                    <Text style={styles.content}>Expand</Text>
                </TouchableHighlight>
            );
        } else {
            return null;
        }
    }

    render() {
        return (
            <View style={styles.jesta}>
                <View style={styles.row}>
                    <Image 
                        source={require('../../../assets/images/profile_blank2.jpg')}
                        style={styles.profilePic} 
                    />
                    <Text style={styles.title}>
                        {this.props.jesta.owner.name}
                    </Text>
                    {this.renderExpandButton()}
                </View>
                <View style={styles.row}>
                    <Text style={styles.content}>
                        {this.props.jesta.title}
                    </Text>
                </View>
                <View style={styles.row}>
                    <Text style={styles.content}>
                        {this.props.jesta.distance}
                    </Text>
                    <Text style={[styles.content, styles.textRed]}>
                        {this.props.jesta.owner.rating}
                    </Text>
                </View>
                {this.renderExpansion()}
                <View style={styles.row}>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Contact</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Save</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Follow</Text>
                    </TouchableHighlight>
                    <TouchableHighlight style={styles.button}>
                        <Text style={styles.content}>Share</Text>
                    </TouchableHighlight>
                </View>
            </View>
        );
    }
}

// JestaBox.propTypes = {
//     jesta: React.PropTypes.shape({
//         owner: React.PropTypes.shape({
//             name: React.PropTypes.string.isRequired,
//             rating: React.PropTypes.string,
//         }),
//         title: React.PropTypes.string.isRequired,
//         distance: React.PropTypes.string.isRequired,
//         title: React.PropTypes.string.isRequired,
//         distance: React.PropTypes.string.isRequired,
//         date: React.PropTypes.string.isRequired,
//         duration: React.PropTypes.string.isRequired,
//         participants: React.PropTypes.string.isRequired,
//         description: React.PropTypes.string.isRequired,
//         skills: React.PropTypes.arrayOf(React.PropTypes.string).isRequired,
//     }).isRequired,
//     onExpand: React.PropTypes.func,
//     isExpand: React.PropTypes.bool,
// };

export default JestaBox;