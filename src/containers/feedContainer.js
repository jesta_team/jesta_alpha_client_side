import React, { Component } from 'react';

import API from '../services/apiService';
import Feed from '../components/feed';

class FeedContainer extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            jestas: require('../statics/jestas.json')
        };

    }

    render() {
        return (
            <Feed jestas={this.state.jestas} />
        );
    }
}

export default FeedContainer;