import React, { Component } from 'react';
import {
    View
} from 'react-native';

import API from '../services/apiService';
import Profile from '../components/profile';

class ProfileContainer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            profile: require('../statics/profile.json')
            // profile: API.getUserProfile("0")
        };

    }

    render() {
        return (
            <Profile profile={this.state.profile} />
        );
    }
}

export default ProfileContainer;